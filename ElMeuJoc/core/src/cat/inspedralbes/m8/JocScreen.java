package cat.inspedralbes.m8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;

import cat.inspedralbes.m8.Constants;

public class JocScreen extends ScreenAdapter{
	
	public static final int AMPLE_COTXE = 150;
	public static final int ALT_COTXE = 250;

	//Referencia
	ElMeuJoc joc;
	
	//Model de dades
	Rectangle player;
	List<Rectangle> cotxes;
	float ultimCotxe = 0;
	
	// Grafics
	Texture playerTexture;
	Texture enemicTexture;
	Texture fonsTexture;
	
	//Sons
	Sound sirena = Gdx.audio.newSound(Gdx.files.internal("sons/sirena.mp3"));
	Sound xoc = Gdx.audio.newSound(Gdx.files.internal("sons/xoc.mp3"));
	
	//HUD puntuació
	int puntuacio;
	BitmapFont hud = new BitmapFont();
	
	public JocScreen(ElMeuJoc joc) {
		
		this.joc = joc;
		this.puntuacio = 0;
		
		player = new Rectangle(0, 0, AMPLE_COTXE, ALT_COTXE);
		
		//Llista de cotxes buida
		cotxes = new ArrayList<Rectangle>();
		
		playerTexture = new Texture(Gdx.files.internal("cotxes/coche_policia.png"));
		enemicTexture = new Texture(Gdx.files.internal("cotxes/coche_lila.png"));
		fonsTexture = new Texture(Gdx.files.internal("carretera.png"));
		
	}
	
	@Override
	public void render (float delta) {
		
		//Gestio input
		gestionarInput();
		
		//Calculs cotxes
		actualitza();
		
		//System.out.println(cotxes);
		
		//Molt important. El dibuxat s'ha de fer entre un begin i un end
		joc.batch.begin();
		
		
		//Dibuixar imatge fons
		joc.batch.draw(fonsTexture, 0 , 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		//Hi ha molts draw
		joc.batch.draw(playerTexture, player.x, player.y, player.width, player.height);
		
		//Dibuxar cotxer
		for(Rectangle cotxe : cotxes) {
			joc.batch.draw(enemicTexture, cotxe.x, cotxe.y, cotxe.width, cotxe.height);
		}
		
		hud.draw(joc.batch, "Puntuació: " + puntuacio, 25, Gdx.graphics.getHeight() - 50); 
		
		joc.batch.end();
	}
	
	
	private void actualitza() {
		
		// Obtenim el temps que ha passat des de l'ultim cotxe dibuixat de pantalla. Es el temps d'un frame.
		float delta = Gdx.graphics.getDeltaTime();
		
		//L'acumulem al temps per controlar si ha d'apareixer un enemic
		ultimCotxe += delta;
		if(ultimCotxe > Constants.TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS) {
			
			//Depenent de si retorn un 1, 2 o 3, mostrara un cotxe en un carril o un altre
			int posicioX = (int) Math.floor(Math.random()*3+1);
			
			int x;
			
			if(posicioX == 1) {
				x = 90;
			} else if (posicioX == 2) {
				x = 290;
			} else {
				x = 490;
			}

			int y=800;

			Rectangle nouCotxe = new Rectangle(x,y,AMPLE_COTXE, ALT_COTXE);
			cotxes.add(nouCotxe);
			
			ultimCotxe = 0; //Molt important resetejar el comptador
		}
		
		//2. Cada cotxe baixa un poc.
		for (Rectangle cotxe : cotxes) {
			cotxe.y -= 5;
		}
		
		//Els cotxes que surten de la pantalla per baix, son eliminats de la List
		//Eliminar d'una llista mentre s'esta recorrent no es pot fer amb un for normal.
		//Una manera es amb iterators
		for (Iterator iterator = cotxes.iterator(); iterator.hasNext();) {
			Rectangle cotxe = (Rectangle) iterator.next();
			//Si es detecta que el cotxe surt de la pantalla, s'esborra de la llista
			if(cotxe.getY() < 0.0) {
				cotxes.remove(0);
				
				//Sumem un punt al marcador
				puntuacio++;
			}
			
			
		}
		
		// Mirem si algun cotxe esta xocant amb el player
		// Les colisions entre rectangles podrien complicar-se un poc
		// Afortunadament, hi ha un metode que diu si dos rectangles es solapen
		for (Rectangle cotxe : cotxes) {
			if (cotxe.overlaps(player)) {
				
				//Es reprodueix un so d'un accident
				xoc.play(1.0f);
				
				// Quan el jugador xoca amb un cotxe, torna al principi
				joc.setScreen(new SplashScreen(joc));

			}
		}
	
	}
	
	
	private void gestionarInput() {
		
		float delta = Gdx.graphics.getDeltaTime();
		
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			player.x -= Constants.VELOCITAT_COTXE_JUGADOR * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			player.x += Constants.VELOCITAT_COTXE_JUGADOR * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.SPACE)) {
			sirena.play(1.0f);
		}
		
		//consultem la mida de la pantalla en pixels
		int amplePantalla = Gdx.graphics.getWidth();
		
		//consultem l'alçada de la pantalla en pxels
		int alturaPantalla = Gdx.graphics.getHeight();
		
		//Comprovacio xoc esquerra
		if(player.x < 0) {
			player.x = 0;
		}
		
		//Comprovacio xoc dreta
		if(player.x > (amplePantalla - player.width)) {
			player.x = amplePantalla - player.width;
		}
		
		//Coomprovacio xot baix
		if(player.y < 0) {
			player.y = 0;
		}
		
		//Comprovacio xoc dalt
		if(player.y > (alturaPantalla - player.height)) {
			player.y = alturaPantalla - player.height;
		}
	}
	
	//S'executa una vegada la final
	@Override
	public void dispose () {
		playerTexture.dispose();
		enemicTexture.dispose();
		fonsTexture.dispose();
	}
}
