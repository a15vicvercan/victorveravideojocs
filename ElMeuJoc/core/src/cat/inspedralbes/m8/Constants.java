package cat.inspedralbes.m8;

public class Constants {
	public static final int TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS = 2;
	public static final int VELOCITAT_COTXE_JUGADOR = 300; // Pixels per segons
	public static final int VELOCITAT_COTXE_ENEMIC = 150; // Pixels per segons
	
	public static final int AMPLE_COTXE = 150;
	public static final int ALT_COTXE = 250;
}