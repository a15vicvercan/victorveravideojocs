package cat.inspedralbes.m8.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.inspedralbes.m8.ElMeuJoc;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.title = "Joc d'esquivar cotxes";
	    config.width = 700;
	    config.height = 800;
		
		new LwjglApplication(new ElMeuJoc(), config);
	}
}
